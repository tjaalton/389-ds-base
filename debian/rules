#!/usr/bin/make -f
# -*- makefile -*-

export DEB_BUILD_MAINT_OPTIONS = hardening=+pie

# Keep track of files we don't install
NOT_INSTALLED :=

REALFILE = \
	bin/cl-dump.pl \
	bin/dbgen.pl \
	bin/ds-logpipe.py \
	bin/logconv.pl \
	bin/repl-monitor.pl \
	sbin/migrate-ds.pl \
	sbin/remove-ds.pl \
	sbin/setup-ds.pl \
	sbin/cleanallruv.pl \
	sbin/fixup-linkedattrs.pl \
	sbin/fixup-memberof.pl \
	sbin/ns-accountstatus.pl \
	sbin/ns-activate.pl \
	sbin/ns-inactivate.pl \
	sbin/ns-newpwpolicy.pl \
	sbin/schema-reload.pl \
	sbin/syntax-validate.pl \
	sbin/usn-tombstone-cleanup.pl \
	sbin/verify-db.pl \
	share/man/man1/dbgen.pl.1 \
	share/man/man1/ds-logpipe.py.1 \
	share/man/man1/logconv.pl.1 \
	share/man/man8/migrate-ds.pl.8 \
	share/man/man8/setup-ds.pl.8 \
	share/man/man8/remove-ds.pl.8

# scripts for handling online servers
REALFILE_ONLINE = \
	sbin/bak2db.pl \
	sbin/db2bak.pl \
	sbin/db2index.pl \
	sbin/db2ldif.pl \
	sbin/ldif2db.pl \
	share/man/man8/bak2db.pl.8 \
	share/man/man8/db2bak.pl.8 \
	share/man/man8/db2index.pl.8 \
	share/man/man8/db2ldif.pl.8 \
	share/man/man8/ldif2db.pl.8

%:
	dh $@ --parallel --with autoreconf,systemd,python3 --builddir build/

override_dh_auto_configure:
	dh_auto_configure -- \
		--with-openldap \
		--with-systemd \
		--with-systemdsystemunitdir=/lib/systemd/system \
		--with-systemdsystemconfdir=/etc/systemd/system \
		--with-systemdgroupname=dirsrv.target \
		--with-tmpfiles-d=/etc/tmpfiles.d \
		--enable-autobind \
		--enable-cmocka

	(cd build && make setup.py)

override_dh_auto_build:
	(cd build && python3 setup.py build)
	python3 src/lib389/setup.py build
	dh_auto_build

override_dh_auto_install:
	(cd build && python3 setup.py install --install-layout=deb --root ../debian/tmp)
	# stupid build
	cp -r build/lib/dirsrvtests/ debian/tmp/usr/lib/python3/dist-packages
	(cd src/lib389 && python3 setup.py install --install-layout=deb --root ../../debian/tmp)

	dh_auto_install --max-parallel=1

override_dh_install:
	# lets do the renaming here afterall, instead of in 389-ds-base.install
	for file in $(REALFILE); do mv -f $(CURDIR)/debian/tmp/usr/$$file \
		$(CURDIR)/debian/tmp/usr/`echo $$file | \
		sed -s 's/\.pl//;s/\.py//'`; \
		done
	# use -online suffix so that they won't overwrite the offline scripts
	for file in $(REALFILE_ONLINE); do mv -f $(CURDIR)/debian/tmp/usr/$$file \
		$(CURDIR)/debian/tmp/usr/`echo $$file | \
		sed -s 's/\.pl/-online/'`; \
		done
	# purge .la files
	find $(CURDIR)/debian/tmp -name "*.la" -type f -exec rm -f "{}" \;
	# fix template script headers
	find $(CURDIR)/debian/tmp/usr/share/dirsrv/script-templates/ -type f -exec perl -pi -e  's,#\{\{PERL-EXEC\}\},#!/usr/bin/perl,' "{}" \;

	# Also get rid of other files which aren't installed. Do not
	# use -f to ensure we notice disappearing files:
	set -e; for file in $(NOT_INSTALLED); do rm debian/tmp/$$file; done

	mkdir -p $(CURDIR)/debian/tmp/etc/systemd/system/dirsrv.target.wants

	dh_install

override_dh_missing:
	dh_missing --fail-missing

override_dh_systemd_enable:
	dh_systemd_enable -p389-ds-base --no-enable dirsrv-snmp.service

override_dh_shlibdeps:
	dh_shlibdeps -l"debian/389-ds-base/usr/lib/$(DEB_HOST_MULTIARCH)/dirsrv" -a
